# `labotekh.labotekh.ws_backup`
The `labotekh.labotekh.ws_backup` role handle the backup generation for all the services.
These backup are stored on an S3 bucket configured in the variables.

The backup policy is defined by the service variables : `backup.dayly`, `backup.weekly`, `backup.monthly`, `backup.yearly`.

## How to create backup configuration

The `backup_save.sh` file placed in the configuration folder of the service will be executed to save the service's database in the `files` folder. This folder is then archived, cyphered and saved to S3. Example of save script with a Django project :
```sh
# backup_save.sh
#!/bin/sh
touch files/backup.json
docker-compose exec -u runner web python3 manage.py dumpdata > files/backup.json
```

## Tips

Prefix the files generated and used by your scripts with `backup` so they can be deleted by cleaning tasks.
